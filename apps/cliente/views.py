from django.shortcuts import render, redirect
from .forms import ClienteForm
from .models import Cliente
from django.views.generic import TemplateView, ListView, UpdateView, CreateView, DeleteView
from django.urls import reverse_lazy
# Create your views here.


class Inicio(TemplateView):
    template_name = 'index.html'


class ListadoCliente(ListView):
    model = Cliente
    template_name = 'cliente/listar_cliente.html' 
    context_object_name = 'clientes'
    queryset = Cliente.objects.filter(estado = True)

    

class ActualizarCliente(UpdateView):
    model = Cliente
    template_name = 'cliente/crear_cliente.html'
    form_class = ClienteForm
    success_url = reverse_lazy('cliente:listar_cliente') 

class CrearCliente(CreateView):
    model = Cliente
    form_class = ClienteForm
    template_name = 'cliente/crear_cliente.html'
    success_url = reverse_lazy('cliente:listar_cliente')

class EliminarCliente(DeleteView):
    model = Cliente
    
    def post(self, request, pk, *args, **kwargs):
        object = Cliente.objects.get(id=pk)
        object.estado = False
        object.save()
        return redirect('cliente:listar_cliente')




