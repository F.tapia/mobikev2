from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import Inicio, ListadoCliente, ActualizarCliente, CrearCliente, EliminarCliente


urlpatterns = [
    path('inicio',login_required(Inicio.as_view()), name='index'),
    path('crear_cliente/', login_required(CrearCliente.as_view()), name = 'crear_cliente'),
    path('listar_cliente/', login_required(ListadoCliente.as_view()), name = 'listar_cliente'),
    path('editar_cliente/<int:pk>', login_required(ActualizarCliente.as_view()), name = 'editar_cliente'),
    path('eliminar_cliente/<int:pk>', login_required(EliminarCliente.as_view()), name = 'eliminar_cliente')
]