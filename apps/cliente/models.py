from django.db import models

# Create your models here.
class Cliente(models.Model):
    id = models.AutoField(primary_key = True)
    rut = models.CharField(max_length = 20, blank=False, null=False)
    apellidos = models.CharField(max_length = 200, blank = False, null = False)
    telefono = models.CharField(max_length = 13,blank=False, null=False)
    correo = models.CharField(max_length = 200, blank = False, null = False)
    sexo = models.CharField(max_length = 20, blank=False, null=False)
    fecha_nacimiento = models.CharField(max_length = 200, blank = False, null = False)
    nombre = models.CharField(max_length = 200, blank = False, null = False)
    direccion = models.CharField(max_length = 200, blank = False, null = False)
    estado = models.BooleanField('Estado', default = True)
    
    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['id']


    def __str__(self):
        return self.nombre


 