from django import forms
from .models import Cliente

class ClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ['rut','apellidos','telefono','correo','sexo','fecha_nacimiento','nombre','direccion']
        labels = {
            'rut': 'Rut del cliente',
            'apellidos': 'Apellido del cliente',
            'telefono': 'Telefono celular del cliente',
            'correo': 'Correo del cliente',
            'sexo': 'Genero del cliente',
            'fecha_nacimiento': 'Fecha de nacimiento del cliente',
            'nombre': 'Nombre del cliente',
            'direccion': 'Direccion del cliente'
        }
        widgets = {
            'rut':forms.TextInput(
                attrs = {
                    
                    'placeholder':'Ingrese rut del cliente'
                }
            ),
            'apellidos':forms.TextInput(
                attrs = {
                    'placeholder':'Ingrese los apellidos del cliente'
                }
            ),
            'telefono':forms.TextInput(
                attrs = {
                    'placeholder':'Ingrese el telefono celular del cliente'
                }
            ),
            'correo':forms.TextInput(
                attrs = {
                    'placeholder':'Ingrese el correo personal del cliente'
                }
            ),
            'sexo':forms.TextInput(
                attrs = {
                    'placeholder':'Ingrese genero del cliente'
                }
            ),
            'fecha_nacimiento':forms.TextInput(
                attrs = {
                    'placeholder':'Ingrese la fecha de nacimiento del cliente'
                }
            ),
            'nombre':forms.TextInput(
                attrs = {
                    'placeholder':'Ingrese el nombre del cliente'
                }
            ),
            'direccion':forms.TextInput(
                attrs = {
                    'placeholder':'Ingrese la direccion del cliente'
                }
            ),
        }
       